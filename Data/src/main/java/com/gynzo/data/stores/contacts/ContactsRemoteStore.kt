package com.gynzo.data.stores.contacts

import com.gynzo.data.entities.ContactEntity
import com.gynzo.data.remotes.ContactsRemote
import io.reactivex.Single
import javax.inject.Inject

class ContactsRemoteStore @Inject constructor(
    private val contactsRemote: ContactsRemote
): ContactsDataStore {

    override fun getContacts(): Single<List<ContactEntity>> {
        return contactsRemote.getMarvelContacts()
    }
}