package com.gynzo.data.stores.contacts

import javax.inject.Inject

class ContactsDataStoreFactory @Inject constructor(
    private val remote: ContactsRemoteStore,
    private val device: ContactsDeviceStore
) {
    fun getRemoteStore(): ContactsDataStore {
        return remote
    }

    fun getDeviceStore(): ContactsDataStore {
        return device
    }
}