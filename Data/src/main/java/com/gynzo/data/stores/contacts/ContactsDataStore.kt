package com.gynzo.data.stores.contacts

import com.gynzo.data.entities.ContactEntity
import io.reactivex.Single

interface ContactsDataStore {
    fun getContacts(): Single<List<ContactEntity>>
}