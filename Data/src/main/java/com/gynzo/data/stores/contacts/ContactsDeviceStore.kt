package com.gynzo.data.stores.contacts

import com.gynzo.data.device.ContactDevice
import com.gynzo.data.entities.ContactEntity
import io.reactivex.Single
import javax.inject.Inject

class ContactsDeviceStore @Inject constructor(
    private val contactDevice: ContactDevice
): ContactsDataStore {
    override fun getContacts(): Single<List<ContactEntity>> {
        return contactDevice.getContactsInDevice()
    }
}