package com.gynzo.data.mappers.contact

import com.gynzo.data.entities.ContactEntity
import com.gynzo.data.mappers.EntityMapper
import com.gynzo.domain.models.Contact
import javax.inject.Inject

class ContactMapper @Inject constructor() : EntityMapper<ContactEntity, Contact> {
    override fun mapFromEntity(entity: ContactEntity): Contact {
        return Contact(
            name = entity.name,
            phoneNumber = entity.phoneNumber,
            avatar = entity.avatar
        )
    }

    override fun mapToEntity(model: Contact): ContactEntity {
        return ContactEntity(
            name = model.name,
            phoneNumber = model.phoneNumber,
            avatar = model.avatar
        )
    }
}