package com.gynzo.data.mappers

interface EntityMapper<E, M> {
    fun mapFromEntity(entity: E): M
    fun mapToEntity(model: M): E
}