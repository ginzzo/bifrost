package com.gynzo.data.device

import com.gynzo.data.entities.ContactEntity
import io.reactivex.Single

interface ContactDevice {
    fun getContactsInDevice(): Single<List<ContactEntity>>
}