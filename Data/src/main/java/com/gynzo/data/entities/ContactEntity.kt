package com.gynzo.data.entities

data class ContactEntity(var name: String = "",
                         var phoneNumber: String = "",
                         var avatar: String = "") {

}