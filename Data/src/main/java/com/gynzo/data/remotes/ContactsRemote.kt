package com.gynzo.data.remotes

import com.gynzo.data.entities.ContactEntity
import io.reactivex.Single

interface ContactsRemote {
    fun getMarvelContacts(): Single<List<ContactEntity>>
}