package com.gynzo.data.repositories

import com.gynzo.data.entities.ContactEntity
import com.gynzo.data.mappers.contact.ContactMapper
import com.gynzo.data.stores.contacts.ContactsDataStoreFactory
import com.gynzo.domain.models.Contact
import com.gynzo.domain.repositories.ContactsRepository
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class ContactsDataRepository @Inject constructor(
    private val factory: ContactsDataStoreFactory,
    private val mapper: ContactMapper
) : ContactsRepository {

    override fun getContacts(): Single<List<Contact>> {
        return Single.zip(factory.getDeviceStore().getContacts(),
            factory.getRemoteStore().getContacts(),
            BiFunction<List<ContactEntity>, List<ContactEntity>, List<ContactEntity>> { deviceContacts, remoteContacts ->
                concatAndSortContacts(deviceContacts, remoteContacts)
            }).map { it.map { contactEntity -> mapper.mapFromEntity(contactEntity) } }
    }

    private fun concatAndSortContacts(
        deviceContacts: List<ContactEntity>,
        remoteContacts: List<ContactEntity>
    ): List<ContactEntity> {
        val contacts = deviceContacts.union(remoteContacts)
        return contacts.sortedWith(compareBy { it.name })
    }

    override fun getMarvelContacts(): Single<List<Contact>> {
        return factory.getRemoteStore().getContacts()
            .map { it.map { contactEntity -> mapper.mapFromEntity(contactEntity) } }
    }
}