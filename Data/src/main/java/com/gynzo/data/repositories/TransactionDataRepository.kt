package com.gynzo.data.repositories

import com.gynzo.domain.models.Transaction
import com.gynzo.domain.repositories.TransactionRepository
import io.reactivex.Completable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TransactionDataRepository @Inject constructor(): TransactionRepository {

    override fun transferAmountToContracts(transaction: Transaction): Completable {
        return Completable.complete().delay(3, TimeUnit.SECONDS)
    }
}