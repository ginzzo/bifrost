package com.gynzo.domain.models

data class Transaction(var contacts: ArrayList<Contact> = arrayListOf(),
                       var amount: Float = 0f) {

}