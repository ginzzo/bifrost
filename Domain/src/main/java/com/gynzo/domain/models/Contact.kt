package com.gynzo.domain.models

data class Contact(var name: String = "",
                   var phoneNumber: String = "",
                   var avatar: String = "") {

    interface ContactItemListener {
        fun onSelectContactItem(contact: Contact)
    }

}