package com.gynzo.domain.usecases.transaction

import com.gynzo.domain.executor.PostExecutionThread
import com.gynzo.domain.models.Transaction
import com.gynzo.domain.repositories.TransactionRepository
import com.gynzo.domain.usecases.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class MakeTransaction @Inject constructor(
    private val repository: TransactionRepository,
    private val postExecutionThread: PostExecutionThread
) : CompletableUseCase<Transaction>(postExecutionThread) {

    override fun buildUseCaseObservable(params: Transaction?): Completable {
        return repository.transferAmountToContracts(params!!)
    }
}