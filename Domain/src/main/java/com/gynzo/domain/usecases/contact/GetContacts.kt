package com.gynzo.domain.usecases.contact

import com.gynzo.domain.executor.PostExecutionThread
import com.gynzo.domain.models.Contact
import com.gynzo.domain.repositories.ContactsRepository
import com.gynzo.domain.usecases.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetContacts @Inject constructor(
    private val repository: ContactsRepository,
    private val postExecutionThread: PostExecutionThread
) : SingleUseCase<List<Contact>, Nothing>(postExecutionThread) {

    override fun buildUseCaseObservable(params: Nothing?): Single<List<Contact>> {
        return repository.getContacts()
    }
}