package com.gynzo.domain.repositories

import com.gynzo.domain.models.Transaction
import io.reactivex.Completable

interface TransactionRepository {
    fun transferAmountToContracts(transaction: Transaction): Completable
}