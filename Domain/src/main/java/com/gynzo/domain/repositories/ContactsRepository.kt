package com.gynzo.domain.repositories

import com.gynzo.domain.models.Contact
import io.reactivex.Single

interface ContactsRepository {
    fun getContacts(): Single<List<Contact>>
    fun getMarvelContacts(): Single<List<Contact>>
}