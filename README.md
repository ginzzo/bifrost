## Bifrost - Android App Test

# Setup the app

This app uses the Marvel Api.The Marvel Public API is an authenticated free api used worldwide. You�ll need an account in the portal and three parameters (apikey, ts and hash) to use their endpoints.
More information:

https://developer.marvel.com/documentation/authorization

After register and get the keys, you must introduce it in the build.gradle inside the Remote Module

# About the App

This app is a payment flow that have three distinct screens:

- Contact list selection screen:
This screen has a list of the first 50 characters from the marvel API, and the user device contacts ordered by name. Each row shows the name, the phone (if applies) and
the avatar. This list let you select various contacts. To show the user device contacts, the user must accept the permission.

- Amount selector screen:
This screen let you write an euro amount of maximum 1000 �. Decimals are allowed.

- Confirmation screen:
This screen has a list of the total amount, the contacts selected in the first screen and, aligned to each
contact, the amount for each (total amount / number of contacts selected).