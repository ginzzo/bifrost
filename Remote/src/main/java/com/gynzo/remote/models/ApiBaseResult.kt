package com.gynzo.remote.models

import com.google.gson.annotations.SerializedName

data class ApiBaseResult<T> constructor(
    @SerializedName("code") var code: Int,
    @SerializedName("status") var status: String,
    @SerializedName("message") var message: String,
    @SerializedName("data") var data: T
)