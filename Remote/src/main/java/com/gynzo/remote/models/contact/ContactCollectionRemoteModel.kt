package com.gynzo.remote.models.contact

import com.google.gson.annotations.SerializedName

data class ContactCollectionRemoteModel constructor(
    @SerializedName("offset") var offset: Int,
    @SerializedName("limit") var limit: Int,
    @SerializedName("total") var total: Int,
    @SerializedName("count") var count: Int,
    @SerializedName("results") var results: ArrayList<ContactRemoteModel>)