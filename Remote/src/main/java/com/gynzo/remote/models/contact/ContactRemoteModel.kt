package com.gynzo.remote.models.contact

import com.google.gson.annotations.SerializedName
import com.gynzo.remote.models.thumbnail.ThumbnailRemoteModel

data class ContactRemoteModel constructor(
    @SerializedName("name") var name: String,
    @SerializedName("thumbnail") var thumbnail: ThumbnailRemoteModel
)