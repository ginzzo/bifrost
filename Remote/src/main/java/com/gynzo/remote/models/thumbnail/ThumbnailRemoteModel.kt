package com.gynzo.remote.models.thumbnail

import com.google.gson.annotations.SerializedName

data class ThumbnailRemoteModel(
    @SerializedName("path") var path: String,
    @SerializedName("extension") var extension: String
)