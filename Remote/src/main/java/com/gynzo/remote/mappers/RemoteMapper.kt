package com.gynzo.remote.mappers

interface RemoteMapper<R, E> {
    fun mapFromEntity(entity: E): R
    fun mapToEntity(remoteModel: R): E
}