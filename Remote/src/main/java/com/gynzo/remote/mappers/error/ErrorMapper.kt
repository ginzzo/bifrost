package com.gynzo.remote.mappers.error

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.gynzo.remote.models.ApiBaseResult
import java.lang.reflect.Type
import javax.inject.Inject

open class ErrorMapper @Inject constructor() {
    private val gson: Gson = Gson()

    fun stringToError(errorBody: String): ApiBaseResult<Nothing> {
        val type: Type = object : TypeToken<ApiBaseResult<Nothing>>() {}.type
        return gson.fromJson(errorBody, type)
    }
}