package com.gynzo.remote.mappers.contact

import com.gynzo.data.entities.ContactEntity
import com.gynzo.remote.mappers.RemoteMapper
import com.gynzo.remote.models.contact.ContactRemoteModel
import com.gynzo.remote.models.thumbnail.ThumbnailRemoteModel
import javax.inject.Inject

class ContactRemoteMapper @Inject constructor() : RemoteMapper<ContactRemoteModel, ContactEntity> {
    override fun mapFromEntity(entity: ContactEntity): ContactRemoteModel {
        return ContactRemoteModel(
            name = entity.name,
            thumbnail = ThumbnailRemoteModel(
                path = entity.avatar,
                extension = ""
            )
        )
    }

    override fun mapToEntity(remoteModel: ContactRemoteModel): ContactEntity {
        return ContactEntity(
            name = remoteModel.name,
            phoneNumber = "",
            avatar = ("${remoteModel.thumbnail.path}.${remoteModel.thumbnail.extension}")
        )
    }
}