package com.gynzo.remote.services

import com.google.gson.Gson
import com.gynzo.remote.models.ApiBaseResult
import com.gynzo.remote.models.contact.ContactCollectionRemoteModel
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface MarvelApiService {
    @GET("v1/public/characters?limit=50&orderBy=name")
    fun getMarvelContacts(@QueryMap authenticationParams: Map<String, String>): Single<Response<ApiBaseResult<ContactCollectionRemoteModel>>>
}