package com.gynzo.remote.helper

import java.security.MessageDigest

class HashHelper {
    companion object {
        private const val HEX_CHARS = "0123456789ABCDEF"

        const val MD5_ALGORITHM_TYPE = "MD5"

        /**
         * Source = https://www.samclarke.com/kotlin-hash-strings/
         */
        fun hashStringByAlgorithm(algorithmType: String, data: String): String {
            val bytes = MessageDigest
                .getInstance(algorithmType)
                .digest(data.toByteArray())

            val result = StringBuilder(bytes.size * 2)

            bytes.forEach {
                val i = it.toInt()
                result.append(HEX_CHARS[i shr 4 and 0x0f])
                result.append(HEX_CHARS[i and 0x0f])
            }

            return result.toString()
        }
    }
}