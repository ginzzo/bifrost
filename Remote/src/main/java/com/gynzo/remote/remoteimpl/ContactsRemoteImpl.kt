package com.gynzo.remote.remoteimpl

import com.gynzo.data.entities.ContactEntity
import com.gynzo.data.remotes.ContactsRemote
import com.gynzo.remote.BuildConfig
import com.gynzo.remote.helper.HashHelper
import com.gynzo.remote.mappers.contact.ContactRemoteMapper
import com.gynzo.remote.mappers.error.ErrorMapper
import com.gynzo.remote.services.MarvelApiService
import io.reactivex.Single
import java.util.*
import javax.inject.Inject

class ContactsRemoteImpl @Inject constructor(
    private var service: MarvelApiService,
    private var errorMapper: ErrorMapper,
    private var contactMapper: ContactRemoteMapper
) : ContactsRemote {
    companion object {
        private const val PARAM_TIMESTAMP_NAME = "ts"
        private const val PARAM_HASH_NAME = "hash"
        private const val PARAM_API_KEY_NAME = "apikey"
    }

    override fun getMarvelContacts(): Single<List<ContactEntity>> {
        return service.getMarvelContacts(getAuthenticationParams())
            .map { response ->
                if (response.isSuccessful) {
                    response.body()!!.data.results.map {
                        contactMapper.mapToEntity(it)
                    }
                } else {
                    val errorBody = errorMapper.stringToError(response.errorBody()!!.string())
                    throw Exception(errorBody.message)
                }
            }
    }

    private fun getAuthenticationParams(): Map<String, String> {
        val authenticationParams = hashMapOf<String, String>()

        val timestamp = getTimestamp()

        authenticationParams.put(PARAM_API_KEY_NAME, BuildConfig.MARVEL_PUBLIC_API_KEY)
        authenticationParams.put(PARAM_TIMESTAMP_NAME, timestamp)
        authenticationParams.put(PARAM_HASH_NAME, generateHash(timestamp))

        return authenticationParams
    }

    private fun getTimestamp(): String {
        return Calendar.getInstance().timeInMillis.toString()
    }

    private fun generateHash(timestamp: String): String {
        val stringToHash = timestamp + BuildConfig.MARVEL_PRIVATE_API_KEY + BuildConfig.MARVEL_PUBLIC_API_KEY
        return HashHelper.hashStringByAlgorithm(HashHelper.MD5_ALGORITHM_TYPE, stringToHash).toLowerCase()
    }
}