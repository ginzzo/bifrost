package com.gynzo.device.mappers

import android.database.Cursor
import android.net.Uri
import com.gynzo.data.entities.ContactEntity
import javax.inject.Inject


class ContactColumnMapper @Inject constructor() {


    fun mapDisplayName(cursor: Cursor, contact: ContactEntity, columnIndex: Int) {
        val displayName = cursor.getString(columnIndex)
        if (displayName != null && !displayName.isEmpty()) {
            contact.name = displayName
        }
    }


    fun mapPhoneNumber(cursor: Cursor, contact: ContactEntity, columnIndex: Int) {
        val phoneNumber = cursor.getString(columnIndex)
        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            contact.phoneNumber = phoneNumber
        }
    }

    fun mapPhoto(cursor: Cursor, contact: ContactEntity, columnIndex: Int) {
        val uri = cursor.getString(columnIndex)
        if (uri != null && !uri!!.isEmpty()) {
            contact.avatar = Uri.parse(uri).toString()
        }
    }

}