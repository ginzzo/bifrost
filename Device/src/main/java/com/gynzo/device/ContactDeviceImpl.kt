package com.gynzo.device

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.provider.ContactsContract
import com.gynzo.data.device.ContactDevice
import com.gynzo.data.entities.ContactEntity
import com.gynzo.device.mappers.ContactColumnMapper
import com.gynzo.domain.models.Contact
import io.reactivex.Single
import javax.inject.Inject


class ContactDeviceImpl @Inject constructor(val context: Context,
                                            val mapper: ContactColumnMapper) : ContactDevice {
    private val contentResolver: ContentResolver = context.contentResolver

    private val PROJECTION = arrayOf(
        ContactsContract.Data.CONTACT_ID,
        ContactsContract.Data.DISPLAY_NAME_PRIMARY,
        ContactsContract.Data.STARRED,
        ContactsContract.Data.PHOTO_URI,
        ContactsContract.Data.PHOTO_THUMBNAIL_URI,
        ContactsContract.Data.DATA1,
        ContactsContract.Data.MIMETYPE,
        ContactsContract.Data.IN_VISIBLE_GROUP
    )

    private fun createCursor(): Cursor {
        return contentResolver.query(
            ContactsContract.Data.CONTENT_URI,
            PROJECTION,
            null, null,
            ContactsContract.Data.CONTACT_ID
        )
    }

    override fun getContactsInDevice(): Single<List<ContactEntity>> {
        return Single.create { emitter ->

            val ids = arrayListOf<Long>()
            val contacts = arrayListOf<ContactEntity>()

            val cursor = createCursor()
            cursor.moveToFirst()

            val idColumnIndex = cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID)
            val displayNamePrimaryColumnIndex = cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME_PRIMARY)
            val photoColumnIndex = cursor.getColumnIndex(ContactsContract.Data.PHOTO_URI)
            val mimeTypeColumnIndex = cursor.getColumnIndex(ContactsContract.Data.MIMETYPE)
            val phoneNumberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)

            while (!cursor.isAfterLast) {
                val id = cursor.getLong(idColumnIndex)

                if (!ids.contains(id)) {
                    var contact = ContactEntity()
                    mapper.mapDisplayName(cursor, contact, displayNamePrimaryColumnIndex)
                    mapper.mapPhoto(cursor, contact, photoColumnIndex)

                    val mimetype = cursor.getString(mimeTypeColumnIndex)

                    when (mimetype) {
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE -> {
                            mapper.mapPhoneNumber(cursor, contact, phoneNumberIndex)
                        }
                    }

                    ids.add(id)
                    contacts.add(contact)
                }

                cursor.moveToNext()
            }

            cursor.close()

            emitter.onSuccess(contacts)
        }
    }
}