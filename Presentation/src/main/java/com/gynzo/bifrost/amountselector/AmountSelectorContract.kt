package com.gynzo.bifrost.amountselector

import com.gynzo.bifrost.base.BasePresenter
import com.gynzo.bifrost.base.BaseView

interface AmountSelectorContract {
    interface View: BaseView<Presenter> {
        fun initKeyboard(maxLengthAmount: Int)
        fun initAmountTransactionChangeListener()
        fun setAmountTextColor(colorId: Int)

    }

    interface Presenter: BasePresenter<View> {
        fun isValidAmount(amount: String): Boolean
        fun getAmountTextColorByMaxValidation(amount: String)
    }
}