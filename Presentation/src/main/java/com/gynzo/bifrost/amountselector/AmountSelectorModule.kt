package com.gynzo.bifrost.amountselector

import com.gynzo.bifrost.di.scopes.FragmentScope
import dagger.Binds
import dagger.Module

@Module
abstract class AmountSelectorModule {

    @Binds
    @FragmentScope
    abstract fun providesAmountSelectorPresenter(presenter: AmountSelectorPresenter): AmountSelectorContract.Presenter
}