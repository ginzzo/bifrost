package com.gynzo.bifrost.amountselector

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gynzo.bifrost.R
import com.gynzo.bifrost.base.BaseFragment
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.fragment_amount_selector.*
import javax.inject.Inject

class AmountSelectorFragment : BaseFragment(), AmountSelectorContract.View, Step {
    @Inject
    lateinit var presenter: AmountSelectorContract.Presenter

    private lateinit var amountSelectorListener: AmountSelectorListener

    interface AmountSelectorListener {
        fun onAmountSelected(amount: Float)
    }

    companion object {
        fun getInstance(): AmountSelectorFragment {
            return AmountSelectorFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_amount_selector, container, false)
    }

    override fun onStart() {
        super.onStart()
        presenter.onViewStart(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is AmountSelectorListener) {
            amountSelectorListener = context as AmountSelectorListener
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroy()
    }

    override fun onSelected() {
        activity!!.setTitle(R.string.set_transaction_amount)
    }

    override fun verifyStep(): VerificationError? {
        if (presenter.isValidAmount(tv_transaction_amount.text.toString())) {
            amountSelectorListener.onAmountSelected(tv_transaction_amount.text.toString().toFloat())
            return null
        } else {
            return VerificationError(context!!.resources.getString(R.string.error_invalid_amount))
        }
    }

    override fun onError(error: VerificationError) {
        super.showToast(error.errorMessage)
    }

    override fun initKeyboard(maxLengthAmount: Int) {
        nk_amount_numeric_keyboard.initTextViewToEdit(tv_transaction_amount, maxLengthAmount)
    }

    override fun setAmountTextColor(colorId: Int) {
        tv_transaction_amount.setTextColor(context!!.resources.getColor(colorId))
    }

    override fun initAmountTransactionChangeListener() {
        tv_transaction_amount.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    presenter.getAmountTextColorByMaxValidation(p0.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }
}
