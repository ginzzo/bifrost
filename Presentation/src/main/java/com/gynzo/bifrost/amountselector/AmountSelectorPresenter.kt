package com.gynzo.bifrost.amountselector

import com.gynzo.bifrost.R
import com.gynzo.bifrost.di.scopes.FragmentScope
import javax.inject.Inject

@FragmentScope
class AmountSelectorPresenter @Inject constructor(): AmountSelectorContract.Presenter {
    private val MAX_LENGTH_AMOUNT = 4
    private val MAX_TRANSACTION_AMOUNT = 1000

    private lateinit var view: AmountSelectorContract.View

    override fun onViewStart(view: AmountSelectorContract.View) {
        this.view = view
        view.initKeyboard(MAX_LENGTH_AMOUNT)
        view.initAmountTransactionChangeListener()
    }

    override fun onViewDestroy() {}

    override fun isValidAmount(amount: String): Boolean {
        return amount.isNotEmpty() && amount.toFloat() <= MAX_TRANSACTION_AMOUNT
    }

    override fun getAmountTextColorByMaxValidation(amount: String) {
        if (amount.toFloat() > MAX_TRANSACTION_AMOUNT) {
            view.setAmountTextColor(R.color.colorAccent)
        } else {
            view.setAmountTextColor(R.color.colorPrimaryDark)
        }
    }
}