package com.gynzo.bifrost.base

interface BasePresenter<V> {
    fun onViewStart(view: V)
    fun onViewDestroy()
}