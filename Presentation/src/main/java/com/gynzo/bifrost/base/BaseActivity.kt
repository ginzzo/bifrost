package com.gynzo.bifrost.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.gynzo.bifrost.custom.dialogs.BifrostProgressDialog
import dagger.android.support.DaggerAppCompatActivity

open class BaseActivity constructor(): DaggerAppCompatActivity() {

    lateinit var progressDialog: BifrostProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        progressDialog = BifrostProgressDialog.getInstance()
    }

    fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    fun showToast(stringId: Int) {
        Toast.makeText(this, stringId, Toast.LENGTH_SHORT).show()
    }

    fun showProgressDialog() {
        progressDialog.show(fragmentManager, BifrostProgressDialog.BIFROST_PROGRESS_DIALOG_TAG)
    }

    fun hideProgressDialog() {
        if (progressDialog.showsDialog) {
            progressDialog.dismiss()
        }
    }

    fun addFragment(fragment: Fragment, containerId: Int) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(containerId, fragment)
        transaction.commit()
    }

    fun addFragmentToStack(fragment: Fragment, containerId: Int) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(containerId, fragment)
        transaction.addToBackStack(fragment::class.simpleName)
        transaction.commit()
    }

    fun hideKeyboard(view: View) {
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_IMPLICIT)
    }

    fun showKeyboard(editText: EditText) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED)
    }
}