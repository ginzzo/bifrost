package com.gynzo.bifrost.base

import android.content.Context
import android.view.View
import android.widget.EditText
import dagger.android.support.DaggerFragment

open class BaseFragment: DaggerFragment() {
    lateinit var baseActivity: BaseActivity

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is BaseActivity) {
            baseActivity = context
        }
    }

    fun showToast(resId: Int) {
        baseActivity.showToast(resId)
    }

    fun showToast(text: String) {
        baseActivity.showToast(text)
    }

    fun showProgressDialog() {
        baseActivity.showProgressDialog()
    }

    fun hideProgressDialog() {
        baseActivity.hideProgressDialog()
    }

    fun hideKeyboard(view: View) {
        baseActivity.hideKeyboard(view)
    }

    fun showKeyboard(view: EditText) {
        baseActivity.showKeyboard(view)
    }

}