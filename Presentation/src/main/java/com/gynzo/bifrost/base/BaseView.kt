package com.gynzo.bifrost.base

interface BaseView<P> {
    fun showToast(text: String)
    fun showToast(stringId: Int)
    fun showProgressDialog()
    fun hideProgressDialog()
}