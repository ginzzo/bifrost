package com.gynzo.bifrost.confirmtransaction

import com.gynzo.bifrost.di.scopes.FragmentScope
import dagger.Binds
import dagger.Module

@Module
abstract class TransactionConfirmationModule {

    @Binds
    @FragmentScope
    abstract fun providesTransactionConfirmationPresenter(presenter: TransactionConfirmationPresenter): TransactionConfirmationContract.Presenter
}