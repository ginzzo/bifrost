package com.gynzo.bifrost.confirmtransaction

import com.gynzo.bifrost.base.BasePresenter
import com.gynzo.bifrost.base.BaseView
import com.gynzo.domain.models.Transaction

interface TransactionConfirmationContract {
    interface View: BaseView<Presenter> {
        fun initTransactionConfirmationList()
        fun showTransactions(transaction: Transaction)
        fun showTotalAmount(totalAmount: String)
    }

    interface Presenter: BasePresenter<View> {
        fun stepSelected(transaction: Transaction)
    }
}