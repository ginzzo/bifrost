package com.gynzo.bifrost.confirmtransaction

import com.gynzo.bifrost.di.scopes.FragmentScope
import com.gynzo.domain.models.Transaction
import javax.inject.Inject

@FragmentScope
class TransactionConfirmationPresenter @Inject constructor(): TransactionConfirmationContract.Presenter {
    private lateinit var view: TransactionConfirmationContract.View

    override fun onViewStart(view: TransactionConfirmationContract.View) {
        this.view = view
        view.initTransactionConfirmationList()
    }

    override fun onViewDestroy() {}

    override fun stepSelected(transaction: Transaction) {
        view.showTransactions(transaction)
        view.showTotalAmount(calculateTotalAmount(transaction))
    }

    private fun calculateTotalAmount(transaction: Transaction): String {
        return (transaction.amount * transaction.contacts.size).toString()
    }
}