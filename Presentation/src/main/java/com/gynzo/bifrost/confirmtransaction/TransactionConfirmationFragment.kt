package com.gynzo.bifrost.confirmtransaction


import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.gynzo.bifrost.R
import com.gynzo.bifrost.base.BaseFragment
import com.gynzo.domain.models.Transaction
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.fragment_transaction_confirmation.*
import java.text.Format
import javax.inject.Inject

class TransactionConfirmationFragment : BaseFragment(), TransactionConfirmationContract.View, Step {
    @Inject
    lateinit var presenter: TransactionConfirmationContract.Presenter

    private lateinit var transactionConfirmationListener: TransactionConfirmationListener

    private lateinit var transactionListAdapter: TransactionListAdapter

    interface TransactionConfirmationListener {
        fun getTransactionInfo(): Transaction
    }

    companion object {
        fun getInstance(): TransactionConfirmationFragment {
            return TransactionConfirmationFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_confirmation, container, false)
    }

    override fun onStart() {
        super.onStart()
        presenter.onViewStart(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is TransactionConfirmationListener) {
            transactionConfirmationListener = context as TransactionConfirmationListener
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroy()
    }

    override fun initTransactionConfirmationList() {
        rv_transaction_list.layoutManager = LinearLayoutManager(context)

        transactionListAdapter = TransactionListAdapter(context!!)
        rv_transaction_list.adapter = transactionListAdapter
    }

    override fun showTotalAmount(totalAmount: String) {
        val totalAmountText = String.format(context!!.resources.getString(R.string.amount_in_euros), totalAmount)
        total_amount.text = totalAmountText
    }

    override fun showTransactions(transaction: Transaction) {
        transactionListAdapter.addTransaction(transaction)
        rv_transaction_list.visibility = VISIBLE
    }

    override fun onSelected() {
        activity!!.setTitle(R.string.transaction_confirmation)
        presenter.stepSelected(transactionConfirmationListener.getTransactionInfo())
    }

    override fun verifyStep(): VerificationError? {
        return null
    }

    override fun onError(error: VerificationError) {
        super.showToast(error.errorMessage)
    }
}
