package com.gynzo.bifrost.confirmtransaction

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.gynzo.bifrost.R
import com.gynzo.domain.models.Transaction

class TransactionListAdapter constructor(
    private val context: Context
) : RecyclerView.Adapter<TransactionListAdapter.ViewHolder>() {

    private var transaction: Transaction = Transaction()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivContactImage = itemView.findViewById<ImageView>(R.id.iv_contact_image)
        val tvContactName = itemView.findViewById<TextView>(R.id.tv_contact_name)
        val tvContactPhone = itemView.findViewById<TextView>(R.id.tv_contact_phone)
        val tvTransactionAmount = itemView.findViewById<TextView>(R.id.tv_transaction_amount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_contact_transaction_list, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return transaction.contacts.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = transaction.contacts[position]

        setAvatar(holder.ivContactImage, contact.avatar)

        holder.tvContactName.text = contact.name
        setPhone(contact.phoneNumber, holder.tvContactPhone)

        setAmount(holder.tvTransactionAmount)
    }

    private fun setAvatar(imageView: ImageView, url: String) {
        Glide.with(context)
            .load(url)
            .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_account_circle))
            .into(imageView)
    }

    private fun setPhone(phoneNumber: String, phoneNumberView: TextView) {
        if (phoneNumber.isNotEmpty() && phoneNumber.isNotBlank()) {
            phoneNumberView.text = phoneNumber
            phoneNumberView.visibility = View.VISIBLE
        } else {
            phoneNumberView.visibility = View.GONE
        }
    }

    private fun setAmount(amountView: TextView) {
        val amountText = String.format(context.resources.getString(R.string.amount_in_euros), transaction.amount.toString())
        amountView.text = amountText
    }

    fun addTransaction(transaction: Transaction) {
        this.transaction = transaction
        notifyDataSetChanged()
    }
}