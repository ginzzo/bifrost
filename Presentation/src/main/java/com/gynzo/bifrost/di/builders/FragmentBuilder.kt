package com.gynzo.bifrost.di.builders

import com.gynzo.bifrost.amountselector.AmountSelectorFragment
import com.gynzo.bifrost.amountselector.AmountSelectorModule
import com.gynzo.bifrost.confirmtransaction.TransactionConfirmationFragment
import com.gynzo.bifrost.confirmtransaction.TransactionConfirmationModule
import com.gynzo.bifrost.contactselector.ContactSelectorFragment
import com.gynzo.bifrost.contactselector.ContactSelectorModule
import com.gynzo.bifrost.di.scopes.FragmentScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {
    @FragmentScope
    @ContributesAndroidInjector(modules = [ContactSelectorModule::class])
    abstract fun contactSelectorFragment(): ContactSelectorFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [AmountSelectorModule::class])
    abstract fun amountSelectorFragment(): AmountSelectorFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [TransactionConfirmationModule::class])
    abstract fun transactionConfirmationFragment(): TransactionConfirmationFragment
}