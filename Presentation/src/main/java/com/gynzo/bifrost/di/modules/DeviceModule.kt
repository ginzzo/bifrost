package com.gynzo.bifrost.di.modules

import com.gynzo.data.device.ContactDevice
import com.gynzo.device.ContactDeviceImpl
import dagger.Binds
import dagger.Module

@Module
abstract class DeviceModule {
    @Binds
    abstract fun bindsContactDevice(contactDeviceImpl: ContactDeviceImpl): ContactDevice
}