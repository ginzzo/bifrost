package com.gynzo.bifrost.di.modules

import com.gynzo.bifrost.UiThread
import com.gynzo.domain.executor.PostExecutionThread
import dagger.Binds
import dagger.Module

@Module
abstract class PresentationModule {
    @Binds
    abstract fun bindPostExecutionThread(uiThread: UiThread): PostExecutionThread
}