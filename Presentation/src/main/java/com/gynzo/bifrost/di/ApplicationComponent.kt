package com.gynzo.bifrost.di

import android.app.Application
import com.gynzo.bifrost.BifrostApplication
import com.gynzo.bifrost.di.builders.ActivityBuilder
import com.gynzo.bifrost.di.builders.FragmentBuilder
import com.gynzo.bifrost.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityBuilder::class,
    FragmentBuilder::class,
    ApplicationModule::class,
    DataModule::class,
    RemoteModule::class,
    DeviceModule::class,
    PresentationModule::class])

interface ApplicationComponent: AndroidInjector<BifrostApplication> {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): ApplicationComponent.Builder

        fun build(): ApplicationComponent
    }
}