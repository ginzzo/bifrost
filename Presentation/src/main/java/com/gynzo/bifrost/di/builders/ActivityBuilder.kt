package com.gynzo.bifrost.di.builders

import com.gynzo.bifrost.di.scopes.ActivityScope
import com.gynzo.bifrost.main.MainActivity
import com.gynzo.bifrost.main.MainModule
import com.gynzo.bifrost.main.MainPresenter
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ActivityScope
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun mainActivity(): MainActivity
}