package com.gynzo.bifrost.di.modules

import com.gynzo.data.remotes.ContactsRemote
import com.gynzo.remote.remoteimpl.ContactsRemoteImpl
import com.gynzo.remote.services.MarvelApiService
import com.gynzo.remote.services.ServiceFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class RemoteModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideMarvelService(): MarvelApiService {
            return ServiceFactory.makeContactService()
        }
    }

    @Binds
    abstract fun providesMarvelContactRemote(remote: ContactsRemoteImpl): ContactsRemote

}