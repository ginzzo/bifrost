package com.gynzo.bifrost.di.modules

import com.gynzo.data.repositories.ContactsDataRepository
import com.gynzo.data.repositories.TransactionDataRepository
import com.gynzo.domain.repositories.ContactsRepository
import com.gynzo.domain.repositories.TransactionRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {
    @Binds
    abstract fun bindContactDataRepository(contactsDataRepository: ContactsDataRepository): ContactsRepository

    @Binds
    abstract fun bindTransactionDataRepository(transactionDataRepository: TransactionDataRepository): TransactionRepository
}