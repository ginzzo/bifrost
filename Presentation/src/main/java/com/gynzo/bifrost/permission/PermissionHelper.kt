package com.gynzo.bifrost.permission

import android.support.v4.app.Fragment

interface PermissionHelper {
    fun isPermissionGranted(permission: String): Boolean
    fun requestPermission(permissions: Array<String>, permissionRequest: Int)
}