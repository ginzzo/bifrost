package com.gynzo.bifrost.permission

import android.Manifest
import android.content.pm.PackageManager
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import javax.inject.Inject

class PermissionHelperImpl @Inject constructor(val fragment: Fragment) : PermissionHelper {
    companion object {
        const val READ_CONTACTS_PERMISSION_REQUEST = 1
    }

    override fun isPermissionGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            fragment.context!!,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun requestPermission(permissions: Array<String>, permissionRequest: Int) {
        fragment.requestPermissions(
            permissions,
            permissionRequest
        )
    }
}