package com.gynzo.bifrost.contactselector

import com.gynzo.bifrost.base.BasePresenter
import com.gynzo.bifrost.base.BaseView
import com.gynzo.domain.models.Contact

interface ContactSelectorContract {
    interface View: BaseView<Presenter> {
        fun initContactList()
        fun showContacts(contacts: List<Contact>)
        fun showContactsNextPage(contacts: List<Contact>)
        fun setNoContactsVisible(isVisible: Boolean)
        fun setProgressBarVisible(isVisible: Boolean)

        fun isReadContactPermissionGranted(): Boolean
        fun requestReadContactPermission()
    }

    interface Presenter: BasePresenter<View> {
        fun getNextPage()
        fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
    }
}