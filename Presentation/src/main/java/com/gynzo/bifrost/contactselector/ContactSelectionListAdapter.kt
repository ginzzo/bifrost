package com.gynzo.bifrost.contactselector

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.gynzo.bifrost.R
import com.gynzo.domain.models.Contact
import org.w3c.dom.Text

class ContactSelectionListAdapter constructor(
    private val context: Context,
    private val contactItemListener: Contact.ContactItemListener) : RecyclerView.Adapter<ContactSelectionListAdapter.ViewHolder>() {

    var contactsList: ArrayList<Contact> = arrayListOf()
    var selectedContacts: List<Contact> = arrayListOf()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivContactImage = itemView.findViewById<ImageView>(R.id.iv_contact_image);
        val tvContactName = itemView.findViewById<TextView>(R.id.tv_contact_name);
        val tvContactPhone = itemView.findViewById<TextView>(R.id.tv_contact_phone);
        val cbContactSelected = itemView.findViewById<CheckBox>(R.id.cb_contact_selected);
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_contact_list, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return contactsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contactsList[position]

        setAvatar(holder.ivContactImage, contact.avatar)

        holder.tvContactName.text = contact.name
        setPhone(contact.phoneNumber, holder.tvContactPhone)

        holder.cbContactSelected.isChecked = selectedContacts.contains(contact)

        holder.cbContactSelected.setOnCheckedChangeListener { compoundButton, isChecked ->
            contactItemListener.onSelectContactItem(contact)
        }
    }

    private fun setAvatar(imageView: ImageView, url: String) {
        Glide.with(context)
            .load(url)
            .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_account_circle))
            .into(imageView)
    }

    private fun setPhone(phoneNumber: String, phoneNumberView: TextView) {
        if(phoneNumber.isNotEmpty() && phoneNumber.isNotBlank()) {
            phoneNumberView.text = phoneNumber
            phoneNumberView.visibility = VISIBLE
        } else {
            phoneNumberView.visibility = GONE
        }
    }

    fun addContacts(contactsList: List<Contact>, selectedContacts: List<Contact>) {
        this.contactsList = ArrayList(contactsList)
        this.selectedContacts = selectedContacts
        notifyDataSetChanged()
    }

    fun addContactsPage(contactsList: List<Contact>) {
        this.contactsList.addAll(contactsList)
        notifyDataSetChanged()
    }
}