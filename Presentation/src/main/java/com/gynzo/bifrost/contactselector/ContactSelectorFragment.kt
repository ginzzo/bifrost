package com.gynzo.bifrost.contactselector


import android.Manifest
import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.gynzo.bifrost.R
import com.gynzo.bifrost.base.BaseFragment
import com.gynzo.bifrost.permission.PermissionHelper
import com.gynzo.bifrost.permission.PermissionHelperImpl.Companion.READ_CONTACTS_PERMISSION_REQUEST
import com.gynzo.domain.models.Contact
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.fragment_contact_selector.*
import javax.inject.Inject

class ContactSelectorFragment : BaseFragment(), ContactSelectorContract.View, Step, Contact.ContactItemListener {
    @Inject
    lateinit var presenter: ContactSelectorContract.Presenter

    @Inject
    lateinit var permissionHelper: PermissionHelper

    private lateinit var contactSelectionListAdapter: ContactSelectionListAdapter
    private var selectedContacts: ArrayList<Contact> = arrayListOf()

    private lateinit var contactSelectorListener: ContactSelectorListener

    interface ContactSelectorListener {
        fun onContactSelectorsStepCompleted(contactsSelected: ArrayList<Contact>)
        fun getSelectedContacts(): ArrayList<Contact>
    }

    companion object {
        fun getInstance(): ContactSelectorFragment {
            return ContactSelectorFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_selector, container, false)
    }

    override fun onStart() {
        super.onStart()
        presenter.onViewStart(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is ContactSelectorListener) {
            contactSelectorListener = context as ContactSelectorListener
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun isReadContactPermissionGranted(): Boolean {
        return permissionHelper.isPermissionGranted(Manifest.permission.READ_CONTACTS)
    }

    override fun requestReadContactPermission() {
        permissionHelper.requestPermission(arrayOf(Manifest.permission.READ_CONTACTS), READ_CONTACTS_PERMISSION_REQUEST)
    }

    override fun onSelected() {
        activity!!.setTitle(R.string.select_contacts_title)
    }

    override fun verifyStep(): VerificationError? {
        if (selectedContacts.isNullOrEmpty()) {
            return VerificationError(context!!.resources.getString(R.string.error_no_contact_selected))
        } else {
            contactSelectorListener.onContactSelectorsStepCompleted(selectedContacts)
            return null
        }
    }

    override fun onError(error: VerificationError) {
        super.showToast(error.errorMessage)
    }

    override fun initContactList() {
        rv_contact_list.layoutManager = LinearLayoutManager(context)

        contactSelectionListAdapter = ContactSelectionListAdapter(context!!, this)
        rv_contact_list.adapter = contactSelectionListAdapter
    }

    override fun showContacts(contacts: List<Contact>) {
        contactSelectionListAdapter.addContacts(contacts, contactSelectorListener.getSelectedContacts())
        rv_contact_list.visibility = VISIBLE
    }

    override fun setNoContactsVisible(isVisible: Boolean) {
        if (isVisible) {
            pb_contacts_progress_bar.visibility = VISIBLE
        } else {
            pb_contacts_progress_bar.visibility = GONE
        }
    }

    override fun setProgressBarVisible(isVisible: Boolean) {
        if (isVisible) {
            tv_no_contacts_found.visibility = VISIBLE
        } else {
            tv_no_contacts_found.visibility = GONE
        }
    }

    override fun showContactsNextPage(contacts: List<Contact>) {
        contactSelectionListAdapter.addContactsPage(contacts)
    }

    override fun onSelectContactItem(contact: Contact) {
        if (selectedContacts.contains(contact)) {
            selectedContacts.remove(contact)
        } else {
            selectedContacts.add(contact)
        }
    }
}
