package com.gynzo.bifrost.contactselector

import com.gynzo.bifrost.permission.PermissionHelper
import com.gynzo.bifrost.permission.PermissionHelperImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class ContactSelectorModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun providePermissionHelper(fragment: ContactSelectorFragment): PermissionHelper {
            return PermissionHelperImpl(fragment)
        }
    }

    @Binds
    abstract fun providesContactSelectorPresenter(presenter: ContactSelectorPresenter):ContactSelectorContract.Presenter
}