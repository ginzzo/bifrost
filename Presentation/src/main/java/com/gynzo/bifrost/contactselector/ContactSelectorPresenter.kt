package com.gynzo.bifrost.contactselector

import android.content.pm.PackageManager
import com.gynzo.bifrost.di.scopes.FragmentScope
import com.gynzo.bifrost.permission.PermissionHelperImpl.Companion.READ_CONTACTS_PERMISSION_REQUEST
import com.gynzo.domain.models.Contact
import com.gynzo.domain.usecases.contact.GetContacts
import com.gynzo.domain.usecases.contact.GetMarvelContacts
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@FragmentScope
class ContactSelectorPresenter @Inject constructor(
    private val getContacts: GetContacts,
    private val getMarvelContacts: GetMarvelContacts) :
    ContactSelectorContract.Presenter {
    private lateinit var view: ContactSelectorContract.View

    override fun onViewStart(view: ContactSelectorContract.View) {
        this.view = view
        view.initContactList()
        checkPermissions()
    }

    override fun onViewDestroy() {
        getMarvelContacts.dispose()
        getContacts.dispose()
    }

    private fun checkPermissions() {
        if (view.isReadContactPermissionGranted()) {
            getContacts()
        } else {
            view.requestReadContactPermission()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            READ_CONTACTS_PERMISSION_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContacts()
                } else {
                    getMarvelContacts()
                }
            }
        }
    }

    private fun getMarvelContacts() {
        getMarvelContacts.execute(GetContactObserver())
    }

    private fun getContacts() {
        getContacts.execute(GetContactObserver())
    }

    override fun getNextPage() {

    }

    inner class GetContactObserver : DisposableSingleObserver<List<Contact>>() {
        override fun onSuccess(t: List<Contact>) {
            view.setProgressBarVisible(false)

            if (t.isEmpty()) {
                view.setNoContactsVisible(true)
            } else {
                view.showContacts(t)
                view.setNoContactsVisible(false)
            }
        }

        override fun onError(e: Throwable) {
            view.showToast(e.message!!)
        }
    }
}