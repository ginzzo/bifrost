package com.gynzo.bifrost.main

import com.gynzo.bifrost.R
import com.gynzo.bifrost.di.scopes.ActivityScope
import com.gynzo.domain.models.Contact
import com.gynzo.domain.models.Transaction
import com.gynzo.domain.usecases.contact.GetMarvelContacts
import com.gynzo.domain.usecases.transaction.MakeTransaction
import io.reactivex.observers.DisposableCompletableObserver
import javax.inject.Inject

@ActivityScope
class MainPresenter @Inject constructor(private val makeTransaction: MakeTransaction): MainContract.Presenter {
    private lateinit var view: MainContract.View

    private val transaction: Transaction = Transaction()

    override fun onViewStart(view: MainContract.View) {
        this.view = view
    }

    override fun onViewDestroy() {
        makeTransaction.dispose()
    }

    override fun storeSelectedContacts(selectedContacts: ArrayList<Contact>) {
        transaction.contacts = selectedContacts
    }

    override fun storeTransactionAmount(amount: Float) {
        transaction.amount = amount
    }

    override fun getTransactionInfo(): Transaction {
        return transaction
    }

    override fun getSelectedContacts(): ArrayList<Contact> {
        return transaction.contacts
    }

    override fun makeTransaction() {
        view.showProgressDialog()
        makeTransaction.execute(MakeTransactionObserver(), transaction)
    }

    private fun clearTransaction() {
        transaction.contacts = arrayListOf()
        transaction.amount = 0f
    }

    inner class MakeTransactionObserver: DisposableCompletableObserver() {
        override fun onComplete() {
            view.hideProgressDialog()

            clearTransaction()

            view.goToFirstStep()
            view.showToast(R.string.transaction_completed)
        }

        override fun onError(e: Throwable) {
            view.hideProgressDialog()
            view.showToast(e.message!!)
        }
    }
}