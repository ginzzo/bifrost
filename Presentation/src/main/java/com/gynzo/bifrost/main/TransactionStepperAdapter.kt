package com.gynzo.bifrost.main

import android.content.Context
import android.support.v4.app.FragmentManager
import com.gynzo.bifrost.amountselector.AmountSelectorFragment
import com.gynzo.bifrost.confirmtransaction.TransactionConfirmationFragment
import com.gynzo.bifrost.contactselector.ContactSelectorFragment
import com.stepstone.stepper.Step
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter
import com.stepstone.stepper.viewmodel.StepViewModel

class TransactionStepperAdapter constructor(
    private val context: Context,
    private val fragmentManager: FragmentManager
) : AbstractFragmentStepAdapter(fragmentManager, context) {

    private val CONTACT_SELECTION_STEPPER_POSITION = 0
    private val AMOUNT_SELECTOR_STEPPER_POSITION = 1
    private val CONFIRMATION_STEPPER_POSITION = 2

    private val TOTAL_STEPS = 3

    override fun getCount(): Int {
        return TOTAL_STEPS
    }

    override fun createStep(position: Int): Step {
        when (position) {
            CONTACT_SELECTION_STEPPER_POSITION -> return ContactSelectorFragment.getInstance()
            AMOUNT_SELECTOR_STEPPER_POSITION -> return AmountSelectorFragment.getInstance()
            CONFIRMATION_STEPPER_POSITION -> return TransactionConfirmationFragment.getInstance()
            else -> return ContactSelectorFragment.getInstance()
        }
    }

    override fun getViewModel(position: Int): StepViewModel {
        return super.getViewModel(position)
    }
}