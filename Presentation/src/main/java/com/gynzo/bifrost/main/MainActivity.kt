package com.gynzo.bifrost.main

import android.os.Bundle
import android.view.View
import com.gynzo.bifrost.R
import com.gynzo.bifrost.amountselector.AmountSelectorFragment
import com.gynzo.bifrost.base.BaseActivity
import com.gynzo.bifrost.confirmtransaction.TransactionConfirmationFragment
import com.gynzo.bifrost.contactselector.ContactSelectorFragment
import com.gynzo.domain.models.Contact
import com.gynzo.domain.models.Transaction
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View, StepperLayout.StepperListener,
    ContactSelectorFragment.ContactSelectorListener, AmountSelectorFragment.AmountSelectorListener,
    TransactionConfirmationFragment.TransactionConfirmationListener {

    @Inject
    lateinit var presenter: MainContract.Presenter

    private var stepperPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initStepper()
    }

    override fun onStart() {
        super.onStart()
        presenter.onViewStart(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroy()
    }

    override fun onBackPressed() {
        if (stepperPosition > 0) {
            stepper_layout.onBackClicked()
        } else {
            super.onBackPressed()
        }
    }

    private fun initStepper() {
        stepper_layout.adapter = TransactionStepperAdapter(context = this, fragmentManager = supportFragmentManager)
        stepper_layout.setListener(this)
    }

    override fun onStepSelected(newStepPosition: Int) {
        stepperPosition = newStepPosition
    }

    override fun onError(verificationError: VerificationError?) {
        super.showToast(verificationError!!.errorMessage)
    }

    override fun onReturn() {}

    override fun onCompleted(completeButton: View?) {
        presenter.makeTransaction()
    }

    override fun onContactSelectorsStepCompleted(contactsSelected: ArrayList<Contact>) {
        presenter.storeSelectedContacts(contactsSelected)
    }

    override fun getSelectedContacts(): ArrayList<Contact> {
        return presenter.getSelectedContacts()
    }

    override fun onAmountSelected(amount: Float) {
        presenter.storeTransactionAmount(amount)
    }

    override fun getTransactionInfo(): Transaction {
        return presenter.getTransactionInfo()
    }

    override fun goToFirstStep() {
        stepper_layout.currentStepPosition = 0
    }
}
