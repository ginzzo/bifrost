package com.gynzo.bifrost.main

import com.gynzo.bifrost.base.BasePresenter
import com.gynzo.bifrost.base.BaseView
import com.gynzo.domain.models.Contact
import com.gynzo.domain.models.Transaction

interface MainContract {
    interface View: BaseView<Presenter> {
        fun goToFirstStep()
    }

    interface Presenter: BasePresenter<View> {
        fun storeSelectedContacts(selectedContacts: ArrayList<Contact>)
        fun storeTransactionAmount(amount: Float)

        fun getTransactionInfo(): Transaction
        fun getSelectedContacts(): ArrayList<Contact>

        fun makeTransaction()
    }
}