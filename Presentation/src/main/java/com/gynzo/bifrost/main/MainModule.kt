package com.gynzo.bifrost.main

import com.gynzo.bifrost.di.scopes.ActivityScope
import dagger.Binds
import dagger.Module

@Module
abstract class MainModule {

    @Binds
    @ActivityScope
    abstract fun providerMainPresenter(presenter: MainPresenter): MainContract.Presenter
}