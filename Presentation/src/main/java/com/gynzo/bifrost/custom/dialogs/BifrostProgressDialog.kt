package com.gynzo.bifrost.custom.dialogs

import android.app.DialogFragment
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gynzo.bifrost.R

class BifrostProgressDialog : DialogFragment() {

    companion object {
        const val BIFROST_PROGRESS_DIALOG_TAG = "bifrost.progress.dialog"

        fun getInstance(): BifrostProgressDialog {
            return BifrostProgressDialog()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val dialogContent = inflater!!.inflate(R.layout.dialog_bifrost_progress_dialog, null)
        dialog.setCancelable(false)
        return dialogContent
    }

    override fun onStart() {
        super.onStart()
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
    }
}