package com.gynzo.bifrost.custom.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import com.gynzo.bifrost.R
import kotlinx.android.synthetic.main.custom_numeric_keyboard.view.*

class NumericKeyboardView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyle, defStyleRes) {

    private lateinit var textKeys: ArrayList<TextView>

    init {
        LayoutInflater.from(context).inflate(R.layout.custom_numeric_keyboard, this)
        initKeyboard()
    }

    private fun initKeyboard() {
        textKeys = arrayListOf()

        textKeys.add(key_0)
        textKeys.add(key_1)
        textKeys.add(key_2)
        textKeys.add(key_3)
        textKeys.add(key_4)
        textKeys.add(key_5)
        textKeys.add(key_6)
        textKeys.add(key_7)
        textKeys.add(key_8)
        textKeys.add(key_9)

        textKeys.add(key_dot)
    }

    fun initTextViewToEdit(textView: TextView, maxNumberSize: Int) {
        for (keyView: TextView in textKeys) {
            keyView.setOnClickListener {
                val currentText = textView.text.toString()
                val textToAdd = keyView.text.toString()

                if (isValidDigitToAdd(currentText, textToAdd, maxNumberSize)) {

                    var text: String

                    if (isDot(textToAdd) && currentText.isEmpty() && currentText.isBlank()) {
                        text = "0$textToAdd"
                    } else {
                        text = currentText + textToAdd
                    }

                    textView.text = text
                }

            }
        }

        key_delete.setOnClickListener {
            if (textView.text.isNotEmpty() && textView.text.isNotBlank()) {
                val text = textView.text.subSequence(0, (textView.text.length - 1))
                textView.text = text
            }
        }
    }

    private fun isValidDigitToAdd(currentText: String, textToAdd: String, maxNumberSize: Int): Boolean {
        if (hasDot(currentText)) {
            if (currentText.length < maxNumberSize + 3) {
                if (!isDot(textToAdd)) {
                    return !hasTwoDecimals(currentText)
                }
            }

            return false

        } else {
            if (!isDot(textToAdd)) {
                return currentText.length < maxNumberSize
            }

            return true
        }
    }

    private fun hasDot(text: String): Boolean {
        return text.contains(context.resources.getString(R.string.key_dot))
    }

    private fun isDot(text: String): Boolean {
        return text == context.resources.getString(R.string.key_dot)
    }

    private fun hasTwoDecimals(text: String): Boolean {
        return text.matches(Regex("\\d+(\\.\\d{2})"))
    }

}